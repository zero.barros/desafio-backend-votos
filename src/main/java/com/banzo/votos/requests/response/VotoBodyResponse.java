package com.banzo.votos.requests.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VotoBodyResponse {

    private AssociadoBodyResponse associado;

    private String voto;
}
