package com.banzo.votos.requests.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SessaoBodyResponse {

    private List<VotoBodyResponse> votos;
}
