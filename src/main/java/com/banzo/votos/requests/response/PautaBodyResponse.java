package com.banzo.votos.requests.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PautaBodyResponse {

    private String id;
    private String nome;
    private SessaoBodyResponse sessao;
    private long quantidadeDeVotosSim;
    private long quantidadeDeVotosNao;

}
