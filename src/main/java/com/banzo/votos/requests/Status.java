package com.banzo.votos.requests;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class Status {

    private String status;

}
