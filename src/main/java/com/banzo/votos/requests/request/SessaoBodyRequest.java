package com.banzo.votos.requests.request;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessaoBodyRequest {

    private List<VotoBodyRequest> votos;
}
