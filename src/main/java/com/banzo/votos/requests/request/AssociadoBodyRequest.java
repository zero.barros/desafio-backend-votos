package com.banzo.votos.requests.request;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AssociadoBodyRequest {

    private String id;

    private String cpf;
}
