package com.banzo.votos.requests.request;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Builder
@Data
public class VotoBodyRequest {

    private AssociadoBodyRequest associado;

    private String voto;
}
