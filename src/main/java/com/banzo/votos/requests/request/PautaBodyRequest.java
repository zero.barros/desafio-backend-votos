package com.banzo.votos.requests.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
public class PautaBodyRequest {

    private String id;

    @NotNull(message = "O campo nome não deverá ser nulo")
    private String nome;

    private SessaoBodyRequest sessao;

}
