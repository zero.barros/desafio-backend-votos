package com.banzo.votos.service.pauta;

import com.banzo.votos.domain.Pauta;
import com.banzo.votos.mapper.PautaMapper;
import com.banzo.votos.repository.PautaRepository;
import com.banzo.votos.requests.Status;
import com.banzo.votos.requests.request.PautaBodyRequest;
import com.banzo.votos.requests.request.VotoBodyRequest;
import com.banzo.votos.requests.response.PautaBodyResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class PautaServiceImpl implements PautaService {

    private final PautaRepository pautaRepository;
    private final WebClient webClient;
    private final String ABLE = "ABLE_TO_VOTE";


    public PautaServiceImpl(PautaRepository pautaRepository, WebClient webClient) {
        this.pautaRepository = pautaRepository;
        this.webClient = webClient;
    }

    @Override
    public PautaBodyResponse salvarPauta(PautaBodyRequest pautaBodyRequest) {

        List<VotoBodyRequest> votos = pautaBodyRequest.getSessao().getVotos();
        List<VotoBodyRequest> votosClean = new ArrayList<>();

        for (VotoBodyRequest v : votos) {
            try {
                Mono<Status> status = this.webClient
                        .get().uri("/{cpf}", v.getAssociado().getCpf())
                        .retrieve()
                        .bodyToMono(Status.class);
                Status s = status.block();

                log.info(s);
                if (s.getStatus().equals(ABLE)) {
                    votosClean.add(v);
                }

            } catch (Exception e){
                log.info(e.getMessage());
            }
        }
        pautaBodyRequest.getSessao().setVotos(votosClean);
        Pauta pauta = PautaMapper.convertRequestToEntity(pautaBodyRequest);
        Pauta pautaSalva = pautaRepository.save(pauta);
        return PautaMapper.convertEntityToResponse(pautaSalva);
    }

    @Override
    public List<PautaBodyResponse> listarPauta() {
        List<Pauta> pautas = pautaRepository.findAll();
        List<PautaBodyResponse> listaPautaResponse = PautaMapper.convertListPautasEntityToListResponse(pautas);
        return listaPautaResponse;
    }

    @Override
    public PautaBodyResponse buscarPautaPorId(String id) {
        Pauta pauta = pautaRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Pauta Não encontrada") );
        return PautaMapper.convertEntityToResponse(pauta);
    }

}
