package com.banzo.votos.service.pauta;

import com.banzo.votos.requests.request.PautaBodyRequest;
import com.banzo.votos.requests.response.PautaBodyResponse;

import java.util.List;

public interface PautaService {

    PautaBodyResponse salvarPauta(PautaBodyRequest pautaBodyRequest);

    List<PautaBodyResponse> listarPauta();

    PautaBodyResponse buscarPautaPorId(String id);
}
