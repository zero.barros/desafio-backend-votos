package com.banzo.votos.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@Builder
public class Voto {

    @Id
    private String id;

    private Associado associado;

    private String voto;
}
