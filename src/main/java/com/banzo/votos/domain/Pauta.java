package com.banzo.votos.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@Builder
public class Pauta {

    @Id
    private String id;

    private String nome;

    private Sessao sessao;
}
