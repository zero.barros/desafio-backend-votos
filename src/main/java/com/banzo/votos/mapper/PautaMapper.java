package com.banzo.votos.mapper;

import com.banzo.votos.domain.Associado;
import com.banzo.votos.domain.Pauta;
import com.banzo.votos.domain.Sessao;
import com.banzo.votos.domain.Voto;
import com.banzo.votos.requests.request.AssociadoBodyRequest;
import com.banzo.votos.requests.request.PautaBodyRequest;
import com.banzo.votos.requests.request.SessaoBodyRequest;
import com.banzo.votos.requests.request.VotoBodyRequest;
import com.banzo.votos.requests.response.AssociadoBodyResponse;
import com.banzo.votos.requests.response.PautaBodyResponse;
import com.banzo.votos.requests.response.SessaoBodyResponse;
import com.banzo.votos.requests.response.VotoBodyResponse;

import java.util.List;
import java.util.stream.Collectors;

public class PautaMapper {

    public static Pauta convertRequestToEntity(PautaBodyRequest pautaBodyRequest) {
        return Pauta
                .builder()
                .nome(pautaBodyRequest.getNome())
                .sessao(convertRequestToEntity(pautaBodyRequest.getSessao()))
                .build();
    }

    public static PautaBodyResponse convertEntityToResponse(Pauta pautaSalva) {
        List<String> list = pautaSalva.getSessao().getVotos()
                .stream()
                .map(Voto::getVoto)
                .collect(Collectors.toList());
        long tam = list.size();
        long sim = list.stream().filter(l -> l.equals("SIM")).count();
        return PautaBodyResponse
                .builder()
                .id(pautaSalva.getId())
                .nome(pautaSalva.getNome())
                .quantidadeDeVotosNao(tam - sim)
                .quantidadeDeVotosSim(sim)
                .sessao(convertEntityToResponse(pautaSalva.getSessao()))
                .build();
    }

    private static SessaoBodyResponse convertEntityToResponse(Sessao sessao) {
        return SessaoBodyResponse
                .builder()
                .votos(sessao.getVotos().stream().map(PautaMapper::convertEntityToResponse).collect(Collectors.toList()))
                .build();
    }

    private static List<VotoBodyResponse> convertListEntityToListResponse(List<Voto> votos) {
        return votos.stream().map(PautaMapper::convertEntityToResponse).collect(Collectors.toList());

    }

    private static VotoBodyResponse convertEntityToResponse(Voto voto) {
        return VotoBodyResponse
                .builder()
                .voto(voto.getVoto())
                .associado(convertRequestToEntity(voto.getAssociado()))
                .build();
    }

    private static AssociadoBodyResponse convertRequestToEntity(Associado associado) {
        return AssociadoBodyResponse
                .builder()
                .id(associado.getId())
                .cpf(associado.getCpf())
                .build();
    }

    private static Sessao convertRequestToEntity(SessaoBodyRequest sessaoRequest) {

        return Sessao
                .builder()
                .votos(convertListRequestToListEntity(sessaoRequest.getVotos()))
                .build();
    }

    private static List<Voto> convertListRequestToListEntity(List<VotoBodyRequest> votosRequest) {
        return votosRequest.stream().map(PautaMapper::convertRequestToEntity).collect(Collectors.toList());
    }

    private static Voto convertRequestToEntity(VotoBodyRequest votoRequest) {
        return Voto
                .builder()
                .associado(convertRequestToEntity(votoRequest.getAssociado()))
                .voto(votoRequest.getVoto())
                .build();
    }

    private static Associado convertRequestToEntity(AssociadoBodyRequest associadoRequest) {
        return Associado
                .builder()
                .id(associadoRequest.getId())
                .cpf(associadoRequest.getCpf())
                .build();
    }

    public static List<PautaBodyResponse> convertListPautasEntityToListResponse (List<Pauta> pautas) {
        return pautas.stream().map(PautaMapper::convertEntityToResponse).collect(Collectors.toList());
    }
}
