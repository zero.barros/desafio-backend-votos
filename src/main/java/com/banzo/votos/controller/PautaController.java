package com.banzo.votos.controller;

import com.banzo.votos.requests.request.PautaBodyRequest;
import com.banzo.votos.requests.response.PautaBodyResponse;
import com.banzo.votos.service.pauta.PautaService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pauta")
public class PautaController {

    private final PautaService pautaService;

    public PautaController(PautaService pautaService) {
        this.pautaService = pautaService;
    }

    @GetMapping
    public ResponseEntity<List<PautaBodyResponse>> listarPauta() {
        List<PautaBodyResponse> pautaBodyResponses = pautaService.listarPauta();
        return ResponseEntity.ok(pautaBodyResponses);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PautaBodyResponse> listarPauta(@PathVariable String id) {
        PautaBodyResponse pautaBodyResponses = pautaService.buscarPautaPorId(id);
        return ResponseEntity.ok(pautaBodyResponses);
    }

    @PostMapping
    public ResponseEntity<PautaBodyResponse> salvarPauta(@RequestBody @Valid PautaBodyRequest pautaBodyRequest) {
        PautaBodyResponse pautaBodyResponse = pautaService.salvarPauta(pautaBodyRequest);
        return ResponseEntity.ok(pautaBodyResponse);
    }
}
