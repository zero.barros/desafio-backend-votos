package com.banzo.votos.repository;

import com.banzo.votos.domain.Pauta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PautaRepository extends MongoRepository<Pauta, String> {
}
