package com.banzo.votos.service.pauta;

import com.banzo.votos.domain.Associado;
import com.banzo.votos.domain.Pauta;
import com.banzo.votos.domain.Sessao;
import com.banzo.votos.domain.Voto;
import com.banzo.votos.repository.PautaRepository;
import com.banzo.votos.requests.response.AssociadoBodyResponse;
import com.banzo.votos.requests.response.PautaBodyResponse;
import com.banzo.votos.requests.response.SessaoBodyResponse;
import com.banzo.votos.requests.response.VotoBodyResponse;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class PautaServiceImplTest {

    @InjectMocks
    private PautaServiceImpl pautaService;

    @Mock
    private PautaRepository pautaRepository;

    @Mock
    private WebClient webClient;

    @Before
    public void setup(){
        mockPauta();
        mockSessao();
        mockVoto();
        mockAssociado();
        mockPautaBodyResponse();
        mockSessaoBodyResponse();
        mockVotoBodyResponse();
        mockAssociadoBodyResponse();
    }

    @Test
    public void listarPautaTest() {
        when(pautaRepository.findAll()).thenReturn(Arrays.asList(mockPauta()));
        List<PautaBodyResponse> pautaBodyResponses = pautaService.listarPauta();
        assertEquals(Arrays.asList(mockPautaBodyResponse()), pautaBodyResponses);

    }

    @Test
    public void buscarPautaPorId(){
        when(pautaRepository.findById(Mockito.anyString())).thenReturn(Optional.of(mockPauta()));
        PautaBodyResponse pautaBodyResponse = pautaService.buscarPautaPorId(Mockito.anyString());
        assertEquals(mockPautaBodyResponse(), pautaBodyResponse);
    }

    public Pauta mockPauta() {
        return Pauta
                .builder()
                .sessao(mockSessao())
                .nome("jose")
                .build();
    }

    public Sessao mockSessao() {
        return Sessao
                .builder()
                .votos(Arrays.asList(mockVoto()))
                .build();
    }

    public Voto mockVoto() {
        return Voto.builder()
                .voto("SIM")
                .associado(mockAssociado())
                .build();
    }

    public Associado mockAssociado() {
        return Associado
                .builder()
                .id("1")
                .cpf("04044364478")
                .build();
    }

    public PautaBodyResponse mockPautaBodyResponse() {
        return PautaBodyResponse
                .builder()
                .nome("jose")
                .quantidadeDeVotosNao(0)
                .quantidadeDeVotosSim(1)
                .sessao(mockSessaoBodyResponse())
                .build();
    }

    public SessaoBodyResponse mockSessaoBodyResponse() {
        return SessaoBodyResponse
                .builder()
                .votos(Arrays.asList(mockVotoBodyResponse()))
                .build();
    }

    public VotoBodyResponse mockVotoBodyResponse() {
        return VotoBodyResponse
                .builder()
                .voto("SIM")
                .associado(mockAssociadoBodyResponse())
                .build();
    }

    public AssociadoBodyResponse mockAssociadoBodyResponse() {
        return AssociadoBodyResponse
                .builder()
                .cpf("04044364478")
                .id("1")
                .build();
    }


}