# Desafio backend votação 
## _Aplicado a vaga de Desenvolvedor na SouthSystem_ 
### Passos do desenvolvimento:

- [x] Análise do documento de requisito
- [x] Criação do modelo
- [x] Implementação das classes de dominio
- [x] Implementação do webclient
- [ ] Testes Unitários
- [ ] Implementação de Testes automatizados
- [ ] incomplete
- [x] completed


### Exemplo de request:

http://localhost:8081/pauta:

    {
        "nome": "Tecnologia da informação é a profissão do futuro?",
        "sessao": {
            "votos": [
                {
                    "associado": {
                        "cpf": "6683039049"
                    },
                    "voto": "SIM"
                },
                {
                    "associado": {
                        "cpf": "77162488048"
                    },
                    "voto": "SIM"
                }
            ]
        }
    }
### Exemplo de response:
http://localhost:8081/pauta/{id}

    {
        "id": "603c25ee0510bd6f6f783d9e",
        "nome": "Tecnologia da informação é a profissão do futuro?",
        "sessao": {
            "votos": [
                {
                    "associado": {
                        "cpf": "77162488048"
                    },
                    "voto": "SIM"
                },
                {
                    "associado": {
                        "cpf": "23155654058"
                    },
                    "voto": "NAO"
                }
            ]
        },
        "quantidadeDeVotosSim": 1,
        "quantidadeDeVotosNao": 1
    }

### Exemplo de response lista:
http://localhost:8081/pauta
    
    [
        {
            "id": "603c25ee0510bd6f6f783d9e",
            "nome": "Tecnologia da informação é a profissão do futuro?",
            "sessao": {
                "votos": [
                    {
                        "associado": {
                        "cpf": "77162488048"
                        },
                        "voto": "SIM"
                    },
                    {
                        "associado": {
                        "cpf": "23155654058"
                    },
                        "voto": "NAO"
            },
            "quantidadeDeVotosSim": 3,
            "quantidadeDeVotosNao": 2
        },
        {
            "id": "603c26509fd9bb4f804d93ec",
            "nome": "Tecnologia da informação é a profissão do futuro?",
            "sessao": {
                "votos": [
                    {
                        "associado": {
                        "cpf": "77162488048"
                    },
                        "voto": "SIM"
                    },
                    {
                        "associado": {
                        "cpf": "23155654058"
                    },
                        "voto": "NAO"
                ]
            },
            "quantidadeDeVotosSim": 3,
            "quantidadeDeVotosNao": 1
       }
    ]